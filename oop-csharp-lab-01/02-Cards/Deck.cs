﻿using System;

namespace Cards
{
    class Deck
    {
        private Card[] cards;

        public Deck()
        {
        }

        public void Initialize()
        {
            cards = new Card[Enum.GetNames(typeof(ItalianSeed)).Length * Enum.GetNames(typeof(ItalianValue)).Length];
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */
            try
            {
                int count = 0;
                foreach (string seme in Enum.GetNames(typeof(ItalianSeed)))
                {
                    foreach (string valore in Enum.GetNames(typeof(ItalianValue)))
                    {
                        cards[count] = new Card(valore, seme);
                        count++;
                    }
                }
            }
            catch (NotImplementedException)
            {
                Console.WriteLine("NotImplementedException\n");
            }
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */

            try
            {
                foreach (Card c in cards)
                {
                    Console.WriteLine(c.ToString());
                }
            }
            catch (NotImplementedException)
            {
                throw new NotImplementedException();
            }

        }

    }


    public Card this[string seme, string valore]
    {
        

    }

    enum ItalianSeed
    {
        DENARI,
        COPPE,
        SPADE,
        BASTONI
    }

    enum ItalianValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }
}
